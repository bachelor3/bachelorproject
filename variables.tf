##################################################################################################
##################################################################################################
### Denne filen inneholder variabler som blir brukt av de andre terraform filene.              ###
### Variablene har beskrivelse i "description" og definert verdi-type i "type"                 ###
### Variablene blir gitt en verdi i "default" men de kan overskrives i yaml-filen              ###
### og i "settings" siden til ci/cd på gitlab.                                                 ###
### En variabel kan være uten "default", denne vil trenge verdien sin fra en av de to stedene  ###
### Du kobler en variabel i denne filen med de andre stedene ved å navngi den på denne måten:  ###
### "rg_name" i variables.tf må skrives som "TF_VAR_RG_NAME" i yaml eller settings             ###
##################################################################################################
##################################################################################################

variable "rg_name" {
  description = "Name of the resource group to be created"
  type        = string
}

variable "location" {
  description = "Region in which to create the resources. Defaults to `West Europe`"
  type        = string
}

variable "application_port" {
  description = "Port that you want to expose to the external load balancer"
  default     = 80
}

variable "managed_image_name" {
  description = "Name of the managed image"
  type        = string
}

variable "managed_image_resource_group_name" {
  description = "Name of the managed image resource group"
  type        = string
}

variable "admin_username" {
  description = "Username for VMs"
  type        = string
}

variable "admin_password" {
  description = "Password for VMs"
  type        = string
}
