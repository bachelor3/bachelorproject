#creates a backend configuration for terraform so it can store the information it creates in azure.

terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstate1645450417"
    container_name       = "tfstate"
    key                  = "terraform.tfstate"
  }
}