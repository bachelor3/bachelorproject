#! /usr/bin/env bash
 
set -e
 
# Install NGINX
echo "UPDATING APT-GET"
sudo apt-get update
sudo apt-get upgrade -y
# Installing stress for test the cpu
echo "INSTALLING STRESS"
sudo apt-get -y install stress
#Installing nginx
echo "INSTALLING NGINX"
sudo apt-get -y install nginx
# Deprovisions the vm and removing uneccesary stuff
echo "DEPROVISONS THE VM"
/usr/sbin/waagent -force -deprovision+user && export HISTSIZE=0 && sync
