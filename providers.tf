# Her legges det inn at vi bruker Azure Recourse Manger og kilden og versjonen til denne.
terraform {
  required_version = ">= 1.0.0"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.90.0"
    }
  }
}

# Setter inn at azure recourse manager er provideren til terraform. deklarerer at det ikke er flere features ved å sette opp en tom features blokk.
provider "azurerm" {
  features {}
}
